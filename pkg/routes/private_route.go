package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/controllers"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/utils"
)

func PrivateRoutes(a *fiber.App) {
	route := a.Group("api/v1")
	route.Post("admin/register", controllers.CreateUser)
	route.Post("admin/login", controllers.SighInUser)
	route.Post("employee/login", controllers.SignInEmployee)

	route.Use(utils.AuthorizationRequired())
	//admin acc
	route.Put("authjwt/admin/acc/update/:user_id", controllers.UpdateUser)
	route.Get("authjwt/admin/acc/me/:user_id", controllers.GetMyUser)

	//admin manage

	route.Get("authjwt/admin/manage/emp/history/:user_id", controllers.GetEmpHistoryWorkList)

	route.Post("authjwt/admin/manage/emp/create", controllers.CreateEmployee)
	route.Delete("authjwt/admin/manage/emp/del/:emp_id", controllers.DeleteEmployee)

	//emp
	route.Post("authjwt/emp/signwork", controllers.EmployeeSignWork)
	route.Get("authjwt/emp/lastwork/:id", controllers.GetMyLastWork)

}
