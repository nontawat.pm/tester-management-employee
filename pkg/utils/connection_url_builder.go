package utils

import (
	"fmt"
	"os"
)

// * ConnectionURLBuilder func for building URL connection.
func ConnectionURLBuilder(n string) (string, error) {
	// * Define URL to connection.
	var url string

	// * Switch given names.
	switch n {
	case "fiber":
		// * URL for Fiber connection.
		serverHost := os.Getenv("SERVER_HOST")
		serverPort := os.Getenv("SERVER_PORT")
		url = fmt.Sprintf("%s:%s", serverHost, serverPort)
	case "redis":
		// * URL for Redis connection.
		// url = fmt.Sprintf(
		// 	"%s:%s",
		// 	os.Getenv("REDIS_HOST"),
		// 	os.Getenv("REDIS_PORT"),
		// )
		url = `127.0.0.1:6379`
	case "mysql":
		// * URL for MySQL connection.
		dbHost := os.Getenv("DB_HOST")
		dbUser := os.Getenv("DB_USER")
		dbPass := os.Getenv("DB_PASS")
		dbName := os.Getenv("DB_NAME")
		dbPort := os.Getenv("DB_PORT")
		dbProtocal := os.Getenv("DB_PROTOCAL")

		url = fmt.Sprintf("%s:%s@%s(%s:%s)/%s?parseTime=true",
			dbUser, dbPass, dbProtocal, dbHost, dbPort, dbName,
		)

	default:
		// ! Return error message.
		return "", fmt.Errorf("connection name '%v' is not supported", n)
	}

	// * Return connection URL.
	return url, nil
}
