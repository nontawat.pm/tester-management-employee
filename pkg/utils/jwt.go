package utils

import (
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v2"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type TokenData struct {
	Token string `json:"token" xml:"token" form:"token"`
}

var secret = os.Getenv("JWT_SECRET_KEY")

func JWTDetoken(c *fiber.Ctx) (jwt.MapClaims, error) {
	t := new(TokenData)

	if err := c.BodyParser(t); err != nil {
		logger.Warn(err.Error())
		c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
		return nil, err
	}

	if t.Token == "" {
		err := fmt.Errorf("unprocessable entity")
		return nil, err
	}

	tokenString := t.Token
	claims := jwt.MapClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secret), nil
	})

	if err != nil {
		return nil, err
	}

	_, ok := token.Claims.(jwt.MapClaims)

	if !ok || !token.Valid {
		return nil, err
	}

	return claims, nil
}

func CreateToken(user_id string, user_type string) (Token, error) {
	var msgToken Token
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["user_id"] = user_id
	claims["user_type"] = user_type
	// claims["user_id"] = userId
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()
	t, err := token.SignedString([]byte(secret))
	if err != nil {
		return msgToken, err
	}
	msgToken.AccessToken = t
	refreshToken := jwt.New(jwt.SigningMethodHS256)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["user_id"] = user_id
	rtClaims["user_type"] = user_type
	// claims["user_id"] = userId
	rtClaims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()
	rt, err := refreshToken.SignedString([]byte(secret))
	if err != nil {
		return msgToken, err
	}
	msgToken.RefreshToken = rt
	return msgToken, nil
}

func AuthorizationRequired() fiber.Handler {
	return jwtware.New(jwtware.Config{
		// Filter:         nil,
		SuccessHandler: AuthSuccess,
		ErrorHandler:   AuthError,
		SigningKey:     []byte(secret),
		// SigningKeys:   nil,
		SigningMethod: "HS256",
		// ContextKey:    nil,
		// Claims:        nil,
		// TokenLookup:   nil,
		// AuthScheme:    nil,
	})
}

func AuthError(c *fiber.Ctx, e error) error {
	c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
		"status":        "Unauthorized",
		"status_code":   fiber.StatusUnauthorized,
		"message_error": e.Error(),
		"result":        nil,
	})
	return nil
}
func AuthSuccess(c *fiber.Ctx) error {
	c.Next()
	return nil
}
