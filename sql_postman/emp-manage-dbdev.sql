-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2022 at 11:03 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emp-manage-dbdev`
--
CREATE DATABASE IF NOT EXISTS `emp-manage-dbdev` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `emp-manage-dbdev`;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `user_id`, `name`, `created_date`, `modified_date`) VALUES
(1, '5f46e5de-2e87-45a8-9863-fe9d794ed742', 'TEST', '2022-01-06 11:03:47', '2022-01-06 11:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salary` int(11) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `start_time_work` time DEFAULT NULL,
  `end_time_work` time DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `day_work_mon` tinyint(1) DEFAULT NULL,
  `day_work_tue` tinyint(1) DEFAULT NULL,
  `day_work_wed` tinyint(1) DEFAULT NULL,
  `day_work_thu` tinyint(1) DEFAULT NULL,
  `day_work_fri` tinyint(1) DEFAULT NULL,
  `day_work_sat` tinyint(1) DEFAULT NULL,
  `day_work_sun` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `role_id`, `dept_id`, `first_name`, `last_name`, `gender`, `phone`, `email`, `password`, `salary`, `birthdate`, `start_time_work`, `end_time_work`, `active`, `user_id`, `day_work_mon`, `day_work_tue`, `day_work_wed`, `day_work_thu`, `day_work_fri`, `day_work_sat`, `day_work_sun`, `created_date`, `modified_date`) VALUES
('00a534a7-243b-4f1f-a6b1-e062a0ef2382', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'empaaa@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 1, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-06 17:22:54', '2022-01-06 17:22:54'),
('7ec1f9c1-d8e2-4622-9d00-55f05bf37b1c', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'empaaaa@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 1, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-07 10:49:20', '2022-01-07 10:49:20'),
('8270f03e-d17a-4070-9006-6c00f0183d7e', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'empaaaaa@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 1, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-07 16:19:29', '2022-01-07 16:19:29'),
('bfc47f81-5290-48ec-ad65-eb887d07f2b4', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'empaa@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 0, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-06 17:19:53', '2022-01-06 17:19:53'),
('e1d54a1d-d578-4c59-bd77-50b6437c5d7b', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'empa@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 0, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-06 17:08:58', '2022-01-06 17:08:58'),
('f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', 1, 1, 'emp', 'suremp', 'male', '0825421057', 'emp@emp.com', 'password', 10000, '1996-01-20', '08:00:00', '17:00:00', 0, '31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 1, 1, 1, 1, 1, 1, 1, '2022-01-06 16:50:09', '2022-01-06 16:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `history_work`
--

CREATE TABLE `history_work` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(255) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_work`
--

INSERT INTO `history_work` (`id`, `emp_id`, `timestamp`, `status`, `type`) VALUES
(1, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 13:19:14', 'startwork', 'employee'),
(2, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 13:19:18', 'startwork', 'employee'),
(3, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 13:19:18', 'startwork', 'employee'),
(4, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 13:46:43', 'startwork', 'employee'),
(5, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 13:47:01', 'startwork', 'employee'),
(6, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-06 23:47:02', 'startwork', 'employee'),
(7, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:29:11', 'startwork', 'employee'),
(8, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:29:29', 'endwork', 'employee'),
(9, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:29:37', 'startwork', 'employee'),
(10, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:34:09', 'startwor', 'employee'),
(11, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:36:00', 'startwork', 'employee'),
(13, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:47:47', 'endwork', 'employee'),
(14, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:47:52', 'startwork', 'employee'),
(15, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-06 15:53:17', 'endwork', 'employee'),
(16, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:54:21', 'startwork', 'employee'),
(17, 'f7fcac5e-c8fc-476e-b373-1068b0aa1fd3', '2022-01-07 15:54:31', 'endwork', 'employee'),
(18, 'e1d54a1d-d578-4c59-bd77-50b6437c5d7b', '2022-01-07 10:48:09', 'startwork', 'employee'),
(19, 'e1d54a1d-d578-4c59-bd77-50b6437c5d7b', '2022-01-07 10:48:09', 'endwork', 'employee');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `user_id`, `name`, `created_date`, `modified_date`) VALUES
(1, '5f46e5de-2e87-45a8-9863-fe9d794ed742', 'TEST ROLE', '2022-01-06 11:04:19', '2022-01-06 11:04:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `gender`, `phone`, `email`, `password`, `birthdate`, `created_date`, `modified_date`) VALUES
('31dc6996-09bb-4fcd-8e45-ab3ba4350e20', 'admin1', 'suradmin1', 'male1', '08254210571', 'admin@admin.com', 'password', '1996-01-02', '2022-01-06 10:51:12', '2022-01-06 15:26:43'),
('5f46e5de-2e87-45a8-9863-fe9d794ed742', 'admin', 'suradmin', 'male', '0825421057', 'admina@admin.com', 'password', '1996-01-01', '2022-01-06 15:17:18', '2022-01-06 15:17:18'),
('d142cba4-81c4-47a9-8b0c-c61d2025beb9', 'admin', 'suradmin', 'male', '0825421057', 'admin3@admin.com', 'password', '1996-01-01', '2022-01-06 10:39:54', '2022-01-06 10:39:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `dept_id` (`dept_id`);

--
-- Indexes for table `history_work`
--
ALTER TABLE `history_work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `history_work`
--
ALTER TABLE `history_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`dept_id`) REFERENCES `department` (`id`);

--
-- Constraints for table `history_work`
--
ALTER TABLE `history_work`
  ADD CONSTRAINT `history_work_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
