package database

import "gitlab.com/nontawat.pm/tester-management-employee.git/app/queries"

type Queries struct {
	*queries.UserQueries // * load queries from User query
	*queries.EmployeeQueries
	*queries.HistoryWorkQueries
}

// * OpenDBConnection func for opening database connection.
func OpenDBConnection() (*Queries, error) {
	// * Define a new MySQL connection.
	db, err := MySQLConnection()
	if err != nil {
		return nil, err
	}

	return &Queries{
		// * Set queries from models:
		UserQueries:        &queries.UserQueries{DB: db},
		EmployeeQueries:    &queries.EmployeeQueries{DB: db},
		HistoryWorkQueries: &queries.HistoryWorkQueries{DB: db},
	}, nil
}
