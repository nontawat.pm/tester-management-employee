package main

import (
	"github.com/gofiber/fiber/v2"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/configs"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/middleware"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/routes"
)

func main() {

	config := configs.FiberConfig()

	app := fiber.New(config)

	middleware.FiberMiddleware(app)

	routes.PrivateRoutes(app)
	routes.PublicRoutes(app)

	app.Listen("127.0.0.1:3000")
}
