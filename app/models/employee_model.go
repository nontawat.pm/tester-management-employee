package models

import "github.com/google/uuid"

type Employee struct {
	Id            uuid.UUID `db:"id" json:"id" validate:"required,uuid"`
	RoleId        int       `db:"role_id" json:"role_id"`
	DeptId        int       `db:"dept_id" json:"dept_id"`
	FirstName     string    `db:"first_name" json:"first_name"`
	LastName      string    `db:"last_name" json:"last_name"`
	Gender        string    `db:"gender" json:"gender"`
	Phone         string    `db:"phone" json:"phone"`
	Email         string    `db:"email" json:"email" validate:"required"`
	Password      string    `db:"password" json:"password"`
	Salary        int       `db:"salary" json:"salary"`
	BirthDate     string    `db:"birthdate" json:"birthdate"`
	StartTimeWork string    `db:"start_time_work" json:"start_time_work"`
	EndTimeWork   string    `db:"end_time_work" json:"end_time_work"`
	Active        bool      `db:"active" json:"active"`
	UserId        string    `db:"user_id" json:"user_id"`

	DayWorkMon bool `db:"day_work_mon" json:"day_work_mon"`
	DayWorkTue bool `db:"day_work_tue" json:"day_work_tue"`
	DayWorkWed bool `db:"day_work_wed" json:"day_work_wed"`
	DayWorkThu bool `db:"day_work_thu" json:"day_work_thu"`
	DayWorkFri bool `db:"day_work_fri" json:"day_work_fri"`
	DayWorkSat bool `db:"day_work_sat" json:"day_work_sat"`
	DayWorkSun bool `db:"day_work_sun" json:"day_work_sun"`

	CreatedDate  string `db:"created_date" json:"created_date"`
	ModifiedDate string `db:"modified_date" json:"modified_date"`
}
