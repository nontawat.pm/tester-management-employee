package models

import "github.com/google/uuid"

type User struct {
	Id           uuid.UUID `db:"id" json:"id" validate:"required,uuid"`
	FirstName    string    `db:"first_name" json:"first_name"`
	LastName     string    `db:"last_name" json:"last_name"`
	Gender       string    `db:"gender" json:"gender"`
	Phone        string    `db:"phone" json:"phone"`
	Email        string    `db:"email" json:"email" validate:"required"`
	Password     string    `db:"password" json:"password"`
	BirthDate    string    `db:"birthdate" json:"birthdate"`
	CreatedDate  string    `db:"created_date" json:"created_date"`
	ModifiedDate string    `db:"modified_date" json:"modified_date"`
}
