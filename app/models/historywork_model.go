package models

type HistoryWork struct {
	Id        int    `db:"id" json:"id"`
	EmpId     string `db:"emp_id" json:"emp_id" validate:"required"`
	Timestamp string `db:"timestamp" json:"timestamp"`
	Status    string `db:"status" json:"status"`
	Ttype     string `db:"type" json:"type"`
}

type HistoryWorkList struct {
	Id        int    `db:"id" json:"id"`
	EmpId     string `db:"emp_id" json:"emp_id" validate:"required"`
	Timestamp string `db:"timestamp" json:"timestamp"`
	Status    string `db:"status" json:"status"`
	Ttype     string `db:"type" json:"type"`

	FirstName string `db:"first_name" json:"first_name"`
	LastName  string `db:"last_name" json:"last_name"`
	Phone     string `db:"phone" json:"phone"`
}
