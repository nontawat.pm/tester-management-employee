package controllers

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/services"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/utils"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

func CreateUser(c *fiber.Ctx) error {

	user := &models.User{}

	if err := c.BodyParser(user); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	validate := utils.NewValidator()
	timeNow := time.Now()
	user.Id = uuid.New()
	user.CreatedDate = timeNow.Format("2006-01-02 15:04:05")
	user.ModifiedDate = timeNow.Format("2006-01-02 15:04:05")

	if err := validate.Struct(user); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": utils.ValidatorErrors(err),
			"result":        nil,
		})
	}

	userDetail, _ := services.GetUserByEmail(user.Email)
	if userDetail != nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": "Email is already used",
			"result":        nil,
		})
	}

	_, err := services.CreateUser(user)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	user.Password = ""
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        user,
	})

}

func SighInUser(c *fiber.Ctx) error {

	user := &models.User{}
	if err := c.BodyParser(user); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	userDetail, err := services.GetUserByEmail(user.Email)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if userDetail == nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusUnauthorized,
			"message_error": "Email not found",
			"result":        nil,
		})
	}

	token, err := utils.CreateToken(userDetail.Id.String(), "user")
	if err != nil {
		logger.Error(err.Error())
		c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
		return nil
	}

	password := user.Password
	encodepassword := userDetail.Password

	if password != encodepassword {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"status":        "failed", //Unauthorized
			"status_code":   fiber.StatusUnauthorized,
			"message_error": "Bad Credentials",
			"result":        nil,
		})
	}

	userDetail.Password = ""
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"token":         token,
		"result":        userDetail,
	})

}

func UpdateUser(c *fiber.Ctx) error {
	user := &models.User{}
	if err := c.BodyParser(user); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	validate := utils.NewValidator()
	timeNow := time.Now()
	user.ModifiedDate = timeNow.Format("2006-01-02 15:04:05")

	user.Id = uuid.MustParse(c.Params("user_id"))

	if err := validate.Struct(user); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": utils.ValidatorErrors(err),
			"result":        nil,
		})
	}

	_, err := services.UpdateUser(c.Params("user_id"), user)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	user.Password = ""

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        user,
	})
}

func GetMyUser(c *fiber.Ctx) error {
	id := c.Params("user_id")

	userDetail, err := services.GetUserById(id)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if userDetail == nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": "Invalid request",
			"result":        nil,
		})
	}

	userDetail.Password = ""
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        userDetail,
	})

}

func CreateEmployee(c *fiber.Ctx) error {

	emp := &models.Employee{}
	if err := c.BodyParser(emp); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	validate := utils.NewValidator()
	timeNow := time.Now()
	emp.Id = uuid.New()
	emp.CreatedDate = timeNow.Format("2006-01-02 15:04:05")
	emp.ModifiedDate = timeNow.Format("2006-01-02 15:04:05")

	if err := validate.Struct(emp); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": utils.ValidatorErrors(err),
			"result":        nil,
		})
	}

	empDetail, _ := services.GetEmployeeByEmail(emp.Email)
	if empDetail != nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": "Email is already used",
			"result":        nil,
		})
	}

	_, err := services.CreateEmployee(emp)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	emp.Password = ""
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        emp,
	})

}

func GetEmpHistoryWorkList(c *fiber.Ctx) error {
	id := c.Params("user_id")

	listHistoryWork, err := services.GetEmpHistoryWorkList(id)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if listHistoryWork == nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "success",
			"status_code":   fiber.StatusOK,
			"message_error": nil,
			"result":        listHistoryWork,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        listHistoryWork,
	})

}

func DeleteEmployee(c *fiber.Ctx) error {

	_, err := services.DeleteEmployee(c.Params("emp_id"))

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        nil,
	})
}
