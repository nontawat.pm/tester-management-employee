package controllers

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/services"
	"gitlab.com/nontawat.pm/tester-management-employee.git/pkg/utils"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

func SignInEmployee(c *fiber.Ctx) error {

	emp := &models.Employee{}

	if err := c.BodyParser(emp); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	empDetail, err := services.GetEmployeeByEmail(emp.Email)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if empDetail == nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusUnauthorized,
			"message_error": "Email not found",
			"result":        nil,
		})
	}

	token, err := utils.CreateToken(empDetail.Id.String(), "user")
	if err != nil {
		logger.Error(err.Error())
		c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
		return nil
	}

	password := emp.Password
	encodepassword := empDetail.Password

	if password != encodepassword {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"status":        "failed", //Unauthorized
			"status_code":   fiber.StatusUnauthorized,
			"message_error": "Bad Credentials",
			"result":        nil,
		})
	}

	empDetail.Password = ""
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"token":         token,
		"result":        empDetail,
	})

}

func EmployeeSignWork(c *fiber.Ctx) error {
	history_work := &models.HistoryWork{}

	if err := c.BodyParser(history_work); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": err.Error(),
			"result":        nil,
		})
	}

	validate := utils.NewValidator()
	timeNow := time.Now()
	history_work.Timestamp = timeNow.Format("2006-01-02 15:04:05")

	if err := validate.Struct(history_work); err != nil {
		logger.Warn(err.Error())
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": utils.ValidatorErrors(err),
			"result":        nil,
		})
	}

	lastWork, err := services.GetLastWork(history_work.EmpId, "employee")
	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if lastWork != nil {
		timeLastWork, _ := time.Parse(time.RFC3339, lastWork.Timestamp)
		timeLastWork = timeLastWork.Add(-time.Hour * 7)
		if (lastWork.Status == history_work.Status || lastWork.Status == "endwork") && timeNow.Year() == timeLastWork.Year() && timeNow.YearDay() == timeLastWork.YearDay() {
			return c.Status(fiber.StatusOK).JSON(fiber.Map{
				"status":        "success",
				"status_code":   fiber.StatusOK,
				"message_error": "You are already sing work",
				"result":        history_work,
			})
		}
	}

	_, err = services.CreateHistoryWork(history_work)

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusCreated,
		"message_error": nil,
		"result":        history_work,
	})
}

func GetMyLastWork(c *fiber.Ctx) error {
	id := c.Params("id")

	lastWork, err := services.GetLastWork(id, "employee")

	if err != nil {
		logger.Error(err.Error())
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusInternalServerError,
			"message_error": "Internal Server Error",
			"result":        nil,
		})
	}

	if lastWork == nil {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"status":        "failed",
			"status_code":   fiber.StatusBadRequest,
			"message_error": "Invalid request",
			"result":        nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":        "success",
		"status_code":   fiber.StatusOK,
		"message_error": nil,
		"result":        lastWork,
	})

}
