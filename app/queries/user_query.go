package queries

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
)

type UserQueries struct {
	*sqlx.DB
}

func (q *UserQueries) GetUserByEmail(email string) (models.User, error) {
	user := models.User{}

	query := "SELECT * FROM users WHERE email=?"

	err := q.Get(&user, query, email)

	if err != nil {
		return user, err
	}

	return user, nil
}

func (q *UserQueries) CreateUser(u *models.User) error {

	query := `INSERT INTO users (id,first_name,last_name,phone,email,password,birthdate,created_date,modified_date,gender) VALUES (?, ?, ?,?, ?, ?, ?, ?, ?,?)`

	_, err := q.Exec(query, u.Id, u.FirstName, u.LastName, u.Phone, u.Email, u.Password, u.BirthDate, u.CreatedDate, u.ModifiedDate, u.Gender)
	if err != nil {
		// * Return only error.
		return err
	}

	// * This query returns nothing.
	return nil
}

func (q *UserQueries) UpdateUser(id string, u *models.User) error {

	query := `UPDATE users SET password = ?, first_name = ?, last_name = ?, gender = ?, phone = ?, birthdate = ?,modified_date = ? WHERE id = ?`

	_, err := q.Exec(query, u.Password, u.FirstName, u.LastName, u.Gender, u.Phone, u.BirthDate, u.ModifiedDate, id)

	if err != nil {
		return err
	}

	return nil
}

func (q *UserQueries) GetUserById(id string) (models.User, error) {
	user := models.User{}

	query := "SELECT * FROM users WHERE id=?"

	err := q.Get(&user, query, id)

	if err != nil {
		return user, err
	}

	return user, nil
}

func (q *HistoryWorkQueries) GetEmpHistoryWorkList(id string) ([]models.HistoryWorkList, error) {
	historyWork := []models.HistoryWorkList{}

	// query := "SELECT history_work.* FROM (SELECT * FROM employees WHERE user_id=?) AS emps LEFT JOIN history_work ON emps.id = history_work.emp_id"
	// query := "SELECT * FROM history_work"
	query := "SELECT history_work.*,emps.first_name,emps.last_name,emps.phone FROM (SELECT * FROM employees WHERE user_id=?) AS emps JOIN history_work ON emps.id = history_work.emp_id"
	err := q.Select(&historyWork, query, id)

	if err != nil {
		return historyWork, err
	}

	return historyWork, nil
}
