package queries

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
)

type HistoryWorkQueries struct {
	*sqlx.DB
}

func (q *HistoryWorkQueries) CreateHistoryWork(work *models.HistoryWork) error {
	query := `INSERT INTO history_work (emp_id,timestamp,status,type) VALUES (?,?,?,?)`

	_, err := q.Exec(query, work.EmpId, work.Timestamp, work.Status, work.Ttype)
	if err != nil {
		// * Return only error.
		return err
	}

	// * This query returns nothing.
	return nil
}

func (q *HistoryWorkQueries) GetLastSignWork(id string, wtype string) (models.HistoryWork, error) {
	historyWork := models.HistoryWork{}
	query := `SELECT * FROM history_work WHERE emp_id = ? AND type = ? ORDER BY timestamp DESC`
	err := q.Get(&historyWork, query, id, wtype)
	if err != nil {
		// * Return only error.
		return historyWork, err
	}
	return historyWork, err
}
