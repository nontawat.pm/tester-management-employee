package queries

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
)

type EmployeeQueries struct {
	*sqlx.DB
}

func (q *EmployeeQueries) CreateEmployee(e *models.Employee) error {

	query := `INSERT INTO employees (id,role_id,dept_id,first_name,last_name,gender,phone,email,password,salary,
		birthdate,start_time_work,end_time_work,active,user_id,
		day_work_mon,day_work_tue,day_work_wed,day_work_thu,day_work_fri,day_work_sat,day_work_sun,created_date,modified_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`

	_, err := q.Exec(query, e.Id, e.RoleId, e.DeptId, e.FirstName, e.LastName, e.Gender, e.Phone, e.Email, e.Password, e.Salary,
		e.BirthDate, e.StartTimeWork, e.EndTimeWork, e.Active, e.UserId,
		e.DayWorkMon, e.DayWorkTue, e.DayWorkWed, e.DayWorkThu, e.DayWorkFri, e.DayWorkSat, e.DayWorkSun, e.CreatedDate, e.ModifiedDate)
	if err != nil {
		// * Return only error.
		return err
	}

	// * This query returns nothing.
	return nil
}

func (q *EmployeeQueries) GetEmployeeByEmail(email string) (models.Employee, error) {
	emp := models.Employee{}

	query := "SELECT * FROM employees WHERE email=?"

	err := q.Get(&emp, query, email)

	if err != nil {
		return emp, err
	}

	return emp, nil
}

func (q *UserQueries) DeleteEmployee(id string) error {

	query := `UPDATE employees SET active = ? WHERE id = ?`

	_, err := q.Exec(query, false, id)

	if err != nil {
		return err
	}

	return nil
}
