package services

import (
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/database"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

func CreateHistoryWork(work *models.HistoryWork) (*models.HistoryWork, error) {
	createWork := &models.HistoryWork{}

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return createWork, err
	}

	if err := db.CreateHistoryWork(work); err != nil {
		logger.Error(err.Error())
		return createWork, err
	}

	return createWork, nil
}

func GetLastWork(id string, wtype string) (*models.HistoryWork, error) {

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	lastSignWork, err := db.GetLastSignWork(id, wtype)

	if err != nil {
		logger.Error((err.Error()))
		if err.Error() == "sql: no rows in result set" {
			return nil, nil
		} else {
			return nil, err
		}
	}

	return &lastSignWork, nil
}
