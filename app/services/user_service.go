package services

import (
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/database"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

func CreateUser(user *models.User) (*models.User, error) {
	createUser := &models.User{}

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return createUser, err
	}

	if err := db.CreateUser(user); err != nil {
		logger.Error(err.Error())
		return createUser, err
	}

	return createUser, nil
}

func GetUserByEmail(email string) (*models.User, error) {

	db, err := database.OpenDBConnection()

	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	user, err := db.GetUserByEmail(email)

	if err != nil {
		logger.Error((err.Error()))
		if err.Error() == "sql: no rows in result set" {

			return nil, nil
		} else {
			return nil, err
		}
	}

	userDetail := &models.User{}
	userDetail.Id = user.Id
	userDetail.FirstName = user.FirstName
	userDetail.LastName = user.LastName
	userDetail.Phone = user.Phone
	userDetail.Gender = user.Gender
	userDetail.Email = user.Email
	userDetail.Password = user.Password
	userDetail.BirthDate = user.BirthDate
	userDetail.CreatedDate = user.CreatedDate
	userDetail.ModifiedDate = user.ModifiedDate
	return userDetail, nil
}

func UpdateUser(id string, user *models.User) (*models.User, error) {

	updateUser := &models.User{}

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return updateUser, err
	}

	if err := db.UpdateUser(id, user); err != nil {
		logger.Error(err.Error())
		return updateUser, err
	}

	return updateUser, nil
}

func GetUserById(id string) (*models.User, error) {
	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	user, err := db.GetUserById(id)

	if err != nil {
		logger.Error((err.Error()))
		if err.Error() == "sql: no rows in result set" {
			return nil, nil
		} else {
			return nil, err
		}
	}

	userDetail := &models.User{}
	userDetail.Id = user.Id
	userDetail.FirstName = user.FirstName
	userDetail.LastName = user.LastName
	userDetail.Phone = user.Phone
	userDetail.Gender = user.Gender
	userDetail.Email = user.Email
	userDetail.Password = user.Password
	userDetail.BirthDate = user.BirthDate
	userDetail.CreatedDate = user.CreatedDate
	userDetail.ModifiedDate = user.ModifiedDate
	return userDetail, nil

}

func GetEmpHistoryWorkList(id string) (*[]models.HistoryWorkList, error) {
	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	listHistoryWork, err := db.GetEmpHistoryWorkList(id)

	if err != nil {
		logger.Error((err.Error()))
		if err.Error() == "sql: no rows in result set" {
			return nil, nil
		} else {
			return nil, err
		}
	}
	return &listHistoryWork, nil

}

func DeleteEmployee(id string) (*models.Employee, error) {

	delEmp := &models.Employee{}

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return delEmp, err
	}

	if err := db.DeleteEmployee(id); err != nil {
		logger.Error(err.Error())
		return delEmp, err
	}

	return delEmp, nil
}
