package services

import (
	"gitlab.com/nontawat.pm/tester-management-employee.git/app/models"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/database"
	"gitlab.com/nontawat.pm/tester-management-employee.git/platform/logger"
)

func GetEmployeeByEmail(email string) (*models.Employee, error) {

	db, err := database.OpenDBConnection()

	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	emp, err := db.GetEmployeeByEmail(email)

	if err != nil {
		logger.Error(err.Error())

		if err.Error() == "sql: no rows in result set" {

			return nil, nil
		} else {
			return nil, err
		}
	} else {

	}

	empDetail := &models.Employee{
		Id:            emp.Id,
		RoleId:        emp.RoleId,
		DeptId:        emp.DeptId,
		FirstName:     emp.FirstName,
		LastName:      emp.LastName,
		Gender:        emp.Gender,
		Phone:         emp.Phone,
		Email:         emp.Email,
		Password:      emp.Password,
		Salary:        emp.Salary,
		BirthDate:     emp.BirthDate,
		StartTimeWork: emp.StartTimeWork,
		EndTimeWork:   emp.EndTimeWork,
		Active:        emp.Active,
		UserId:        emp.UserId,
		DayWorkMon:    emp.DayWorkMon,
		DayWorkTue:    emp.DayWorkTue,
		DayWorkWed:    emp.DayWorkWed,
		DayWorkThu:    emp.DayWorkThu,
		DayWorkFri:    emp.DayWorkFri,
		DayWorkSat:    emp.DayWorkSat,
		DayWorkSun:    emp.DayWorkSun,
		CreatedDate:   emp.CreatedDate,
		ModifiedDate:  emp.ModifiedDate,
	}

	// empDetail := &models.Employee{}
	// empDetail.Id = emp.Id
	return empDetail, nil
}

func CreateEmployee(emp *models.Employee) (*models.Employee, error) {
	createEmp := &models.Employee{}

	db, err := database.OpenDBConnection()
	if err != nil {
		logger.Error(err.Error())
		return createEmp, err
	}

	if err := db.CreateEmployee(emp); err != nil {
		logger.Error(err.Error())
		return createEmp, err
	}

	return createEmp, nil
}
